//------------------------------------------------------------------------------
// <auto-generated>
//    Этот код был создан из шаблона.
//
//    Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//    Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProcCheck.EFModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class CKL_OTCHET
    {
        public int NOM_MASH { get; set; }
        public System.DateTime BTIME { get; set; }
        public Nullable<long> T_DVIJ_BG { get; set; }
        public Nullable<decimal> L_DVIJ_BG { get; set; }
        public Nullable<long> T_PROSTOI_BG { get; set; }
        public Nullable<long> T_POGRUZKA { get; set; }
        public Nullable<decimal> M_POGRUZKA { get; set; }
        public Nullable<long> T_PROSTOI_SG { get; set; }
        public Nullable<long> T_DVIJ_SG { get; set; }
        public Nullable<decimal> L_DVIJ_SG { get; set; }
        public Nullable<long> T_CIKL { get; set; }
        public Nullable<long> NOM_RAIS { get; set; }
        public Nullable<long> NOM_SMENA { get; set; }
        public Nullable<decimal> SPEED_VAL { get; set; }
        public Nullable<decimal> OBOROTS { get; set; }
        public Nullable<decimal> NARABOTKA_DVS { get; set; }
    }
}
