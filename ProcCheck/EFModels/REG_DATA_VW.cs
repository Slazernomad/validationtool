//------------------------------------------------------------------------------
// <auto-generated>
//    Этот код был создан из шаблона.
//
//    Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//    Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProcCheck.EFModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class REG_DATA_VW
    {
        public string ID_REG { get; set; }
        public short CAR_ID { get; set; }
        public long LOG_ID { get; set; }
        public System.DateTime DATE_REG { get; set; }
        public short TYME_VALID { get; set; }
        public short STATUS { get; set; }
        public System.DateTime DATE_SAVE { get; set; }
        public decimal THERM { get; set; }
    }
}
