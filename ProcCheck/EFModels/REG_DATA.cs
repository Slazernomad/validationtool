//------------------------------------------------------------------------------
// <auto-generated>
//    Этот код был создан из шаблона.
//
//    Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//    Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProcCheck.EFModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class REG_DATA
    {
        public REG_DATA()
        {
            this.REG_DATA_CAN_2 = new HashSet<REG_DATA_CAN_2>();
            this.REG_DATA_CAN = new HashSet<REG_DATA_CAN>();
            this.REG_DATA_GPS = new HashSet<REG_DATA_GPS>();
            this.REG_DATA_MESS = new HashSet<REG_DATA_MESS>();
        }
    
        public short CAR_ID { get; set; }
        public long LOG_ID { get; set; }
        public System.DateTime DATE_REG { get; set; }
        public short TYME_VALID { get; set; }
        public short STATUS { get; set; }
        public System.DateTime DATE_SAVE { get; set; }
        public decimal THERM { get; set; }
        public Nullable<short> MONTH_REG { get; set; }
    
        public virtual CAR CAR { get; set; }
        public virtual ICollection<REG_DATA_CAN_2> REG_DATA_CAN_2 { get; set; }
        public virtual ICollection<REG_DATA_CAN> REG_DATA_CAN { get; set; }
        public virtual ICollection<REG_DATA_GPS> REG_DATA_GPS { get; set; }
        public virtual ICollection<REG_DATA_MESS> REG_DATA_MESS { get; set; }
        public virtual REG_DATA_OUTPUT REG_DATA_OUTPUT { get; set; }
    }
}
