﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using ProcCheck.EFModels;
using ProcCheck.Models;

namespace ProcCheck.Repository
{
    public class Repository
    {
        public List<CKL_OTCHET_VW> GetReportData(DateTime BegTime, DateTime EndTime, long carId,
            decimal morePayload = -1, decimal lessPayload = -1)
        {
            IEnumerable<CKL_OTCHET_VW> result = new List<CKL_OTCHET_VW>();
            var nomMash = new OracleParameter("nom_mash_cur", OracleDbType.Int32, carId,
                ParameterDirection.Input);

            var begTime = new OracleParameter("begin_time_in", OracleDbType.Varchar2, BegTime,
                ParameterDirection.Input);

            var endTime = new OracleParameter("end_time_in", OracleDbType.Varchar2, EndTime, ParameterDirection.Input);



            
            using (var entity = new OracleNewEntities())
            {
                entity.Database.ExecuteSqlCommand(
                    "BEGIN MONITOR.OTCHET_PKG.sett(:nom_mash_cur, :begin_time_in, :end_time_in); end;", nomMash, begTime,
                    endTime);

                var otchet = entity.CKL_OTCHET.ToList();


                return entity.CKL_OTCHET.Select(newReport => new CKL_OTCHET_VW()
                {
                    NOM_MASH = newReport.NOM_MASH,
                    BTIME = newReport.BTIME,
                    T_DVIJ_BG = newReport.T_DVIJ_BG,
                    L_DVIJ_BG = newReport.L_DVIJ_BG,
                    T_PROSTOI_BG = newReport.T_PROSTOI_BG,
                    T_POGRUZKA = newReport.T_POGRUZKA,
                    M_POGRUZKA = newReport.M_POGRUZKA,
                    T_PROSTOI_SG = newReport.T_PROSTOI_SG,
                    T_DVIJ_SG = newReport.T_DVIJ_SG,
                    L_DVIJ_SG = newReport.L_DVIJ_SG,
                    T_CIKL = newReport.T_CIKL,
                    NOM_RAIS = newReport.NOM_RAIS,
                    NOM_SMENA = newReport.NOM_SMENA,
                    SPEED_VAL = newReport.SPEED_VAL,
                    OBOROTS = newReport.OBOROTS,
                    NARABOTKA_DVS = newReport.NARABOTKA_DVS
                }).ToList();

            }
            
        }
    }
}
