﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcCheck.EFModels;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using ProcCheck.Models;


namespace ProcCheck
{
    
    internal class ProcCheck { 
        public static void Main()
        {
           var metReport = new Repository.Repository();
            {
                DateTime reportBegTime;
                DateTime reportEndTime;
                long carId;

                Console.WriteLine();
                Console.WriteLine("Введите время начала интервала для отчета:");
                

                if (!DateTime.TryParse(Console.ReadLine(), out reportBegTime))
                    Console.WriteLine("Неверно введены данные!");

                Console.WriteLine();
                Console.WriteLine("Введите время конца интервала для отчета:");
                

                if (!DateTime.TryParse(Console.ReadLine(), out reportEndTime))
                    Console.WriteLine("Неверно введены данные!");

                Console.WriteLine();
                Console.WriteLine("Введите ID машины:");
                

                if (!long.TryParse(Console.ReadLine(), out carId))
                    Console.WriteLine("Неверно введены данные!");

                DateTime startProc = DateTime.Now;
                var reportData = metReport.GetReportData(reportBegTime, reportEndTime, carId);
                DateTime endProc = DateTime.Now;
                TimeSpan timeOfProc = endProc - startProc;

                Console.WriteLine();
                Console.WriteLine("Процедура получения данных для отчетов была выполнена за время: " + timeOfProc.ToString());
                foreach (var b in reportData)
                {
                    Console.WriteLine();
                    Console.WriteLine("NOM_MASH: " + b.NOM_MASH );
                    Console.WriteLine("BTIME: " + b.BTIME );
                    Console.WriteLine("T_DVIJ_BG: " + b.T_DVIJ_BG );
                    Console.WriteLine("L_DVIJ_BG: " + b.L_DVIJ_BG);
                    Console.WriteLine("T_PROSTOI_BG: " + b.T_PROSTOI_BG);
                    Console.WriteLine("T_POGRUZKA: " + b.T_POGRUZKA);
                    Console.WriteLine("M_POGRUZKA: " + b.M_POGRUZKA);
                    Console.WriteLine("T_PROSTOI_SG: " + b.T_PROSTOI_SG);
                    Console.WriteLine("T_DVIJ_SG: " + b.T_DVIJ_SG);
                    Console.WriteLine("L_DVIJ_SG: " + b.L_DVIJ_SG);
                    Console.WriteLine("T_CIKL: " + b.T_CIKL);
                    Console.WriteLine("NOM_RAIS: " + b.NOM_RAIS);
                    Console.WriteLine("NOM_SMENA: " + b.NOM_SMENA);
                    Console.WriteLine("SPEED_VAL: " + b.SPEED_VAL);
                    Console.WriteLine("OBOROTS: " + b.OBOROTS);
                    Console.WriteLine("NARABOTKA_DVS: " + b.NARABOTKA_DVS);
             }
                Console.WriteLine("Нажмите любую клавишу для продолжения");
                Console.ReadKey();
            }
         
        }


    }
}
