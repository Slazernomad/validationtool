﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcCheck.Models
{
   public partial class CKL_OTCHET_VW
    {
        public CKL_OTCHET_VW()
        {}

        public int NOM_MASH { get; set; }
        public System.DateTime BTIME { get; set; }
        public Nullable<long> T_DVIJ_BG { get; set; }
        public Nullable<decimal> L_DVIJ_BG { get; set; }
        public Nullable<long> T_PROSTOI_BG { get; set; }
        public Nullable<long> T_POGRUZKA { get; set; }
        public Nullable<decimal> M_POGRUZKA { get; set; }
        public Nullable<long> T_PROSTOI_SG { get; set; }
        public Nullable<long> T_DVIJ_SG { get; set; }
        public Nullable<decimal> L_DVIJ_SG { get; set; }
        public Nullable<long> T_CIKL { get; set; }
        public Nullable<long> NOM_RAIS { get; set; }
        public Nullable<long> NOM_SMENA { get; set; }
        public Nullable<decimal> SPEED_VAL { get; set; }
        public Nullable<decimal> OBOROTS { get; set; }
        public Nullable<decimal> NARABOTKA_DVS { get; set; }

        public override string ToString()
        {
            return

                "NOM_MASH: " + NOM_MASH
                + " BTIME: " + BTIME
                + " T_DVIJ_BG: " + T_DVIJ_BG
                + " L_DVIJ_BG: " + L_DVIJ_BG
                + " T_PROSTOI_BG: " + T_PROSTOI_BG
                + " T_POGRUZKA: " + T_POGRUZKA
                + " M_POGRUZKA: " + M_POGRUZKA
                + " T_PROSTOI_SG: " + T_PROSTOI_SG
                + " T_DVIJ_SG: " + T_DVIJ_SG
                + " L_DVIJ_SG: " + L_DVIJ_SG
                + " T_CIKL: " + T_CIKL
                + " NOM_RAIS: " + NOM_RAIS
                + " NOM_SMENA: " + NOM_SMENA
                + " SPEED_VAL: " + SPEED_VAL
                + " OBOROTS: " + OBOROTS
                + " NARABOTKA_DVS: " + NARABOTKA_DVS;
        }
    }
}
